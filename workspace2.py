import nltk
import requests
import bs4

from nltk import FreqDist
from nltk.corpus import stopwords

# Scrape from URL and Create Soup Object then stored in String Var
url = "http://www.rsh-duesseldorf.de/aktuelles/"
r = requests.get(url)
requests.models.Response
html = r.text
BeautifulSoup = bs4.BeautifulSoup
soup = BeautifulSoup(html, "html5lib")
text = soup.get_text()

tokens = nltk.word_tokenize(text)

german_stopwords = set(stopwords.words("german")) # NLTK LIBRARY STOPWORDS
english_stopwords = set(stopwords.words("english")) # NLTK LIBRARY STOPWORDS
custom_stopwords = set((u".", ",",  u"mehr",
                        u":", u"Der",u"Die",u"heute",
                        u"beim",u"lassen",u"lassen",u"woher",u"russisch",
                        u"youtube",u"deutsch",u"synonym","\xc3\xbcbersetzung",
                        u"wer",u"wann",u"viele",u"immer","2018",
                        u"spanisch",u"wohin","f\xc3\xbcr","dass","duden",
                        u"x",u"zwei",u"geht",u"gehen","2",
                        u"bleiben",u"stehen",u"steht",u"wegen","2")) # EIGENE AUTOCOMPLETE SPEZIFISCHE

all_stopwords = german_stopwords

fltrd_autocompleted = []

for w in tokens:
    if w not in all_stopwords:
        fltrd_autocompleted.append(w)

frequency = FreqDist(fltrd_autocompleted)

print (frequency.most_common(100))




