#JE SUIS AUTOCOMPLETE CORPUS ANALYSE

import itertools
import csv
import nltk



######### CORPUS EINLESEN
autocompleted = []

with open("jesuisautocomplete_ger.csv") as csvfile:
       reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC) 
       for row in reader: # JEDE ZEILE IST EIN AUTO VERVOLLSTÄNDIGTER SATZ
           autocompleted.append(row)

l_autocompleted = list(itertools.chain.from_iterable(autocompleted))
#########



def process_content():
    try:
        for i in l_autocompleted:
            words=nltk.word_tokenize(i)
            tagged=nltk.pos_tag(words,tagset=None,lang="ger")
            print(tagged)
    
    except Exception as e:
        print(str(e))

process_content()



