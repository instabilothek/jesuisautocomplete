#JE SUIS AUTOCOMPLETE CORPUS ANALYSE

import itertools
import csv
from nltk import FreqDist

######### CORPUS EINLESEN
autocompleted = []
 
with open("jesuisautocomplete_ger.csv") as csvfile:
       reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC) 
       for row in reader: # JEDE ZEILE IST EIN AUTO VERVOLLSTÄNDIGTER SATZ
           autocompleted.append(row)

l_autocompleted = list(itertools.chain.from_iterable(autocompleted))

str_autocompleted = " ".join(l_autocompleted)

tkn_autocompleted = str_autocompleted.split() # TOKENS BILDEN
##########


frequency = FreqDist(tkn_autocompleted)

print (frequency.most_common(100))





