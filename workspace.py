import nltk
from nltk.tokenize import word_tokenize
import pickle
import re
import itertools
import csv




######### CORPUS EINLESEN
autocompleted = []

with open("jesuisautocomplete_ger.csv") as csvfile:
       reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC) 
       for row in reader: # JEDE ZEILE IST EIN AUTO VERVOLLSTÄNDIGTER SATZ
           autocompleted.append(row)

l_autocompleted = list(itertools.chain.from_iterable(autocompleted))


from ClassifierBasedGermanTagger import ClassifierBasedGermanTagger

with open('nltk_german_classifier_data.pickle', 'rb') as f:
    tagger = pickle.load(f)

regex = re.compile(".....")
    
def create_french_to_universal_dict():
    french_to_universal = {}
    french_to_universal[u"ADJA"]        = u"ADJ"
    french_to_universal[u"ADJD"]        = u"ADJ"
    french_to_universal[u"ADV"]         = u"ADV"
    french_to_universal[u"APPR"]        = u"ADV"
    french_to_universal[u"APPRART"]     = u"KON"    
    french_to_universal[u"APPO"]        = u"KON"
    french_to_universal[u"APZR"]        = u"KON"
    french_to_universal[u"ART"]         = u"KON"
    french_to_universal[u"CARD"]        = u"ZAHL"
    french_to_universal[u"FM"]          = u"NOMEN"
    french_to_universal[u"ITJ"]         = u"PT"
    french_to_universal[u"KON"]         = u"KON"
    french_to_universal[u"KOKOM"]       = u"KON"
    french_to_universal[u"KOUI"]        = u"KON"
    french_to_universal[u"KOUS"]        = u"KON"
    french_to_universal[u"NA"]          = u"NOMEN"
    french_to_universal[u"NE"]          = u"NOMEN"
    french_to_universal[u"NN"]          = u"NOMEN"
    french_to_universal[u"PAV"]         = u"PRON"
    french_to_universal[u"PAVREL"]      = u"KON"
    french_to_universal[u"PDAT"]        = u"KON"
    french_to_universal[u"PDS"]         = u"KON"
    french_to_universal[u"PIAT"]        = u"KON"
    french_to_universal[u"PIS"]         = u"PRON"
    french_to_universal[u"PPER"]        = u"PRON"
    french_to_universal[u"PRF"]         = u"KON"
    french_to_universal[u"PPOSS"]       = u"KON"
    french_to_universal[u"PPOSAT"]      = u"KON"
    french_to_universal[u"PRELAT"]      = u"KON"
    french_to_universal[u"PRELS"]       = u"KON"
    french_to_universal[u"PTKA"]        = u"KON"
    french_to_universal[u"PTKANT"]      = u"PT"
    french_to_universal[u"PTKNEG"]      = u"PTNEG"
    french_to_universal[u"PTKREL"]      = u"PT"
    french_to_universal[u"PTKVZ"]       = u"KON"
    french_to_universal[u"PTKZU"]       = u"KON"
    french_to_universal[u"PWS"]         = u"PRON"
    french_to_universal[u"PWAT"]        = u"KON"
    french_to_universal[u"PWAV"]        = u"KON"
    french_to_universal[u"PWAVREL"]     = u"KON"
    french_to_universal[u"PWREL"]       = u"KON"
    french_to_universal[u"VAFIN"]       = u"VERB"
    french_to_universal[u"VAIMP"]       = u"VERB"
    french_to_universal[u"VS"]          = u"VERB"
    french_to_universal[u"VAINF"]       = u"VERB"
    french_to_universal[u"VAPP"]        = u"VERB"
    french_to_universal[u"VMFIN"]       = u"VERB"
    french_to_universal[u"VS"]          = u"VERB"
    french_to_universal[u"VMINF"]       = u"VERB"
    french_to_universal[u"VPP"]         = u"VERB"
    french_to_universal[u"VMPP"]        = u"VERB"
    french_to_universal[u"VVFIN"]       = u"VERB"
    french_to_universal[u"VVIMP"]       = u"VERB"
    french_to_universal[u"VVINF"]       = u"VERB"
    french_to_universal[u"VVIZU"]       = u"VERB"
    french_to_universal[u"VVPP"]        = u"VERB"
    french_to_universal[u"VVPP"]        = u"VERB"
    french_to_universal[u"VPP"]         = u"VERB"
    french_to_universal[u"VPR"]         = u"VERB"
    french_to_universal[u"VS"]          = u"VERB"
    french_to_universal[regex]          = u"???"
    
    return french_to_universal

french_to_universal_dict = create_french_to_universal_dict()

def map_french_tag_to_universal(list_of_french_tag_tuples):
    return [ (tup[0], french_to_universal_dict[ tup[1] ]) for tup in list_of_french_tag_tuples ] 
    

def process_content():
    try:
        for i in l_autocompleted:
            words = nltk.word_tokenize(i)
            tagged = tagger.tag(words)
            #print(tagged)
            print(map_french_tag_to_universal(tagged))
            
    except Exception as e:
        print(str(e))
        
        
process_content()